# Liga Sabidão

## Aplicativo oficial da Liga Sabidão - Cartola FC

* Acompanhe rankings e outras informações da liga facilmente sem precisar de usuário e senha.
* Para participar da liga e usar o aplicativo, basta entrar no site do Cartola FC e adicionar seu time a liga: Sabidão.

## Licença

[MIT License](http://jamesclebio.mit-license.org/) © James Clébio

