module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
			default: {
				scripts: {
					main: [
						'bower_components/jquery/dist/jquery.js',
						'assets/default/scripts/main.core.js',
						'assets/default/scripts/main.go-roll.js',
						'assets/default/scripts/main.go-top.js',
						'assets/default/scripts/main.alert.js',
						'assets/default/scripts/main.base.js',
						'assets/default/scripts/main.setup.js',
						'assets/default/scripts/main.launcher.js',
						'assets/default/scripts/main.init.js',
					]
				},
				styles: {
					path: 'assets/default/styles',
					compile: '<%= project.default.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},
			default_main: ['assets/default/scripts/**/*']
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			default_main: {
				src: '<%= project.default.scripts.main %>',
				dest: 'deploy/assets/default/scripts/main.js'
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'compressed',
				raw: 'preferred_syntax = :sass'
			},
			default: {
				options: {
					sassDir: '<%= project.default.styles.path %>',
					specify: '<%= project.default.styles.compile %>',
					cssDir: 'deploy/assets/default/styles',
					imagesDir: 'deploy/assets/default/images',
					javascriptsDir: 'deploy/assets/default/scripts',
					fontsDir: 'deploy/assets/default/fonts'
				}
			}
		},

		watch: {
			options: {
				livereload: true,
				spawn: false
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			default_scripts_main: {
				files: ['<%= project.default.scripts.main %>'],
				tasks: ['jshint:default_main', 'uglify:default_main']
			},
			default_styles: {
				files: ['<%= project.default.styles.compile %>'],
				tasks: ['compass:default']
			},
			app: {
				files: ['deploy/**/*.html']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'uglify', 'compass']);
};
