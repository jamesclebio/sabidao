;(function($, window, document, undefined) {
  'use strict';

  main.goRoll = function(target) {
    var $parent = $('body, html'),
      $target = $(target),
      diff = 0;

    if (!$target.length) {
      $target = $parent;
    }

    $parent.animate({
      scrollTop: $target.offset().top + diff
    });
  };
}(jQuery, this, this.document));
