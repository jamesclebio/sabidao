;(function($, window, document, undefined) {
  'use strict';

  main.setup = {
    build: function() {

      // Preferences
      this.setTeam('gostosinho-fc');
      this.setLeague('sabidao');
      this.setPagination(10);

      // Bootstrap
      main.init('launcher');
    },

    getTeam: function() {
      return localStorage.getItem('team');
    },

    setTeam: function(team) {
      localStorage.setItem('team', team);
    },

    getLeague: function() {
      return localStorage.getItem('league');
    },

    setLeague: function(league) {
      localStorage.setItem('league', league);
    },

    getLeagueTeamList: function() {
      return JSON.parse(localStorage.getItem('leagueTeamList'));
    },

    setLeagueTeamList: function(league_team_list) {
      localStorage.setItem('leagueTeamList', JSON.stringify(league_team_list));
    },

    getPagination: function() {
      return localStorage.getItem('pagination');
    },

    setPagination: function(pagination) {
      localStorage.setItem('pagination', pagination);
    },

    init: function() {
      var that = this;

      that.build();
    }
  };
}(jQuery, this, this.document));
