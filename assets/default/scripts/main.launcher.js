;(function($, window, document, undefined) {
  'use strict';

  main.launcher = {
    build: function(partial) {
      var that = this,
        $action_ranking = $(main.base.action_ranking),
        $global_viewer = $(main.base.global_viewer),
        orderby = $action_ranking.val();

      main.base.viewer('views/ranking.html', function () {
        if (orderby === 'parcial') {
          if (partial) {
            main.base.league_team_list_temp = main.setup.getLeagueTeamList();
            that.leaguePartialRound(0);
          } else {
            that.partial();
          }
        } else {
          that.league(orderby, 1);
        }
      });
    },

    league: function(orderby, page) {
      var that = this,
        $action_ranking = $(main.base.action_ranking),
        $global_viewer = $(main.base.global_viewer),
        $table_head = $global_viewer.find('table thead'),
        $table_body = $global_viewer.find('table tbody'),
        label_total = (orderby === 'patrimonio') ? 'Cartoletas' : 'Pontos',
        data = {
          local: 'http://localhost:8080/cartolafc-api/project/deploy/view/league/name/' + localStorage.getItem('league') + '/orderby/' + orderby + '/page/' + page,
          production: 'http://api.jamesclebio.com.br/cartolafc/view/league/name/' + localStorage.getItem('league') + '/orderby/' + orderby + '/page/' + page
        },
        url = (/localhost[\/,:]/.test(window.location.href)) ? data.local : data.production;

      main.base.xhr = $.ajax({
        url: url,
        dataType: 'json',

        beforeSend: function () {
          if (main.base.league_team_pass) {
            main.base.loaderShow(true, 'Inicializando aplicativo');
          } else {
            main.base.loaderShow(true, 'Atualizando ranking');
          }
        },

        error: function () {
          if (main.setup.getLeagueTeamList()) {
            that.partial(true);
            $action_ranking.val('parcial');
          } else {
            main.base.errorShow(true, '<p><strong>Ops! Algo deu errado... :(</strong></p><p>O aplicativo não conseguiu carregar as informações da liga.</p>');
            main.base.loaderShow(false);
          }
        },

        success: function(data) {
          main.base.league_team_list_temp = main.base.league_team_list_temp.concat(data.times);

          if (data.page.total > 0 && data.page.atual <= data.page.total && data.page.atual < +localStorage.getItem('pagination')) {
            that.league($action_ranking.val(), (+data.page.atual + 1));
          } else {
            $table_head.append('<tr><th class="align-center">Posição</th><th>Time</th><th class="align-center">' + label_total + '</th></tr>');
            that.leagueItemPush(main.base.league_team_list_temp);
          }
        }
      });
    },

    leaguePartialRound: function(team) {
      var that = this,
        $global_viewer = $(main.base.global_viewer),
        $table_head = $global_viewer.find('table thead'),
        $table_body = $global_viewer.find('table tbody'),
        data = {
          local: 'http://localhost:8080/cartolafc-api/project/deploy/view/team/name/' + main.base.league_team_list_temp[team].time.slug,
          production: 'http://api.jamesclebio.com.br/cartolafc/view/team/name/' + main.base.league_team_list_temp[team].time.slug
        },
        url = (/localhost[\/,:]/.test(window.location.href)) ? data.local : data.production;

      function positionUpdate(data) {
        for (var i in data) {
          data[i].posicao = parseInt(i) + 1;
        }

        return data;
      }

      main.base.xhr = $.ajax({
        url: url,
        dataType: 'json',

        beforeSend: function () {
          main.base.loaderShow(true, 'Atualizando time <strong>' + (team + 1) + '</strong> de <strong>' + main.base.league_team_list_temp.length + '</strong>', '<button class="action-previous button width-220">Cancelar</button>');
        },

        error: function () {
          main.base.loaderShow(false);
          main.base.errorShow(true, '<p><strong>Ops! Algo deu errado... :(</strong></p><p>O aplicativo não conseguiu carregar as informações da liga.</p>');
        },

        success: function(data) {
          if (data.errors) {
            main.base.league_team_list_temp[team].pontos_ou_patrimonio = 0;
          } else {
            main.base.league_team_list_temp[team].pontos_ou_patrimonio = data.time.pontuacao;
          }

          if (team < (main.base.league_team_list_temp.length - 1)) {
            that.leaguePartialRound(team + 1);
          } else {
            $table_head.append('<tr><th class="align-center">Posição</th><th>Time</th><th class="align-center">Pontos</th></tr>');

            that.leagueItemPush(positionUpdate(main.base.league_team_list_temp.sort(function(obj1, obj2) {
              return obj2.pontos_ou_patrimonio - obj1.pontos_ou_patrimonio;
            })), true);
          }
        }
      });
    },

    leaguePartialRanking: function () {},

    playersPartial: function () {
      var that = this;

      main.base.viewer('views/players.html', function () {
        var $action_ranking = $(main.base.action_ranking),
          $global_viewer = $(main.base.global_viewer),
          $table_head = $global_viewer.find('table thead'),
          $table_body = $global_viewer.find('table tbody'),
          data = {
            local: 'http://localhost:8080/cartolafc-api/project/deploy/round/players',
            production: 'http://api.jamesclebio.com.br/cartolafc/round/players'
          },
          url = (/localhost[\/,:]/.test(window.location.href)) ? data.local : data.production;

        $.ajax({
          url: url,
          dataType: 'json',

          beforeSend: function () {
            $action_ranking.val('').find('option:first-child').attr('selected', 'selected');
            main.base.loaderShow(true, 'Atualizando jogadores', '<button class="action-previous button width-220">Cancelar</button>');
            main.base.errorShow(false);
          },

          error: function () {
            main.base.errorShow(true, '<p><strong>Ops! Algo deu errado... :(</strong></p><p>O aplicativo não conseguiu carregar as informações dos jogadores.</p>');
          },

          success: function(data) {
            var tableHead = '',
                tableBody = '',
                atletaProvavel = null,
                atletaPontos = null;

            if (data.errors) {
              $global_viewer.append('<div class="block-alert block-alert-empty"><div class="text"><p><strong>Ops! Parece cedo...</strong></p><p>O Cartola FC ainda não liberou as parciais da rodada.</p></div></div>');
            } else {
              $table_head.append('<tr><th class="align-center">Função</th><th>Nome</th><th class="align-center">Pontos</th></tr>');

              for (var i in data)  {

                // Score
                switch (true) {
                  case (data[i].pontuacao > 0):
                    atletaPontos = '<span class="data-score up">' + data[i].pontuacao.toFixed(2) + '</span>';
                    break;

                  case (data[i].pontuacao < 0):
                    atletaPontos = '<span class="data-score down">' + data[i].pontuacao.toFixed(2).toString().replace('-', '') + '</span>';
                    break;

                  case (data[i].pontuacao === 0):
                    atletaPontos = '<span class="data-score zero">' + data[i].pontuacao.toFixed(2) + '</span>';
                    break;

                  default:
                    atletaPontos = '<span class="data-score null"></span>';
                    break;
                }

                tableBody += '<tr><td class="align-center"><span class="tag tag-position tag-position-' + data[i].posicao.abreviacao.toLowerCase() + '">' + data[i].posicao.abreviacao + '</span></td><td><strong>' + data[i].apelido + '</strong><br><span class="text-xsmall text-uppercase">' + data[i].clube.nome + '</span></td><td class="align-center">' + atletaPontos + '</td></tr>';
              }

              $table_body.append(tableBody);

              $(main.base.filterPlayerPosition).find('li:first-child a').trigger('click');
            }
          },

          complete: function () {
            main.base.teamSelect();
            main.base.viewerShow(true);
            main.base.loaderShow(false);
          }
        });
      });
    },

    leagueItemPush: function(data, partial) {
      var that = this,
        $table_body = $(main.base.global_viewer).find('table tbody'),
        tableRows = '',
        LeagueTeamList = [],
        position_previous = 0,
        timeVariacao = null;

      for (var i in data) {

        // Variation format
        switch (true) {
          case (+data[i].variacao > 0):
            timeVariacao = '<span class="variation up">' + data[i].variacao + '</span>';
            break;

          case (+data[i].variacao < 0):
            timeVariacao = '<span class="variation down">' + data[i].variacao.toString().replace('-', '') + '</span>';
            break;

          default:
            timeVariacao = '';
            break;
        }

        // Table rows
        if (+data[i].posicao === (+position_previous + 1)) {
          tableRows += '<tr data-view-team="' + data[i].time.slug + '|' + data[i].time.nome + '|' + data[i].time.nome_cartola + '"><td class="align-center"><span class="position">' + data[i].posicao + timeVariacao + '</span></td><td><strong>' + data[i].time.nome + '</strong><br>' + data[i].time.nome_cartola + '</td><td class="align-center"><strong>' + Number(data[i].pontos_ou_patrimonio).toFixed(2) + '</strong></td></tr>';
          position_previous = data[i].posicao;
          LeagueTeamList.push(data[i]);
        }
      }

      $table_body.html(tableRows);

      // League team list (first pass only)
      if (main.base.league_team_pass) {
        main.base.leagueTeamList(LeagueTeamList);
        main.base.league_team_pass = false;
      }

      if (partial) {

        // Set league team list partial
        main.base.league_team_list_partial = LeagueTeamList;
      } else {

        // Update league team list local storage
        main.setup.setLeagueTeamList(LeagueTeamList);
      }

      // Hide loader
      main.base.viewerShow(true);
      main.base.loaderShow(false);
    },

    team: function(team) {
      var that = this;

      main.base.viewer('views/team.html', function () {
        var $action_ranking = $(main.base.action_ranking),
          $global_viewer = $(main.base.global_viewer),
          $header = $global_viewer.find('header'),
          $table_head = $global_viewer.find('table thead'),
          $table_body = $global_viewer.find('table tbody'),
          slug = team.replace(/(.+)\|(.+)\|(.+)/, "$1"),
          data = {
            local: 'http://localhost:8080/cartolafc-api/project/deploy/view/team/name/' + slug,
            production: 'http://api.jamesclebio.com.br/cartolafc/view/team/name/' + slug
          },
          url = (/localhost[\/,:]/.test(window.location.href)) ? data.local : data.production;

        $.ajax({
          url: url,
          dataType: 'json',

          beforeSend: function () {
            $action_ranking.val('').find('option:first-child').attr('selected', 'selected');
            main.base.loaderShow(true, 'Atualizando time', '<button class="action-previous button width-220">Cancelar</button>');
            main.base.errorShow(false);
          },

          error: function () {
            main.base.errorShow(true, '<p><strong>Ops! Algo deu errado... :(</strong></p><p>O aplicativo não conseguiu carregar as informações do time.</p>');
          },

          success: function(data) {
            var header = '',
                tableHead = '',
                tableBody = '',
                atletaProvavel = null,
                atletaPontos = null,
                playerboxUp = 0,
                playerboxDown = 0,
                playerboxZero = 0,
                playerboxNull = 0;

            header += '<h2 class="heading-main">' + team.replace(/(.+)\|(.+)\|(.+)/, "$2") + '</h2>';
            header += '<h4 class="heading-sub">' + team.replace(/(.+)\|(.+)\|(.+)/, "$3") + '</h4>';

            if (data.errors) {
              $header.append(header);
              $global_viewer.append('<div class="block-alert block-alert-empty"><div class="text"><p><strong>Ops! Time não escalado...</strong></p><p>O cartoleiro '+ team.replace(/(.+)\|(.+)\|(.+)/, "$3") + ' precisa cuidar melhor de seu time!</p></div></div>');
            } else {
              header += '<ul class="list-headbox"><li><strong>Rodada ' + data.time.rodada + '</strong></li><li> $' + Number(data.time.patrimonio).toFixed(2) + '</li><li>' + main.base.team_formation[data.time.esquema] + '</li><li class="highlight">' + Number(data.time.pontuacao).toFixed(2) + '</li></ul>';
              header += '<div class="clearfix"></div>';
              header += '<ul class="list-playerbox"><li>Positivo: <strong data-playerbox-up></strong></li><li>Negativo: <strong data-playerbox-down></strong></li><li>Zerado: <strong data-playerbox-zero></strong></li><li>Não jogou: <strong data-playerbox-null></strong></li></ul>';
              $header.append(header);
              $table_head.append('<tr><th class="align-center">Função</th><th>Nome</th><th class="align-center">Pontos</th></tr>');

              for (var i in data.atleta)  {

                // Score
                switch (true) {
                  case (data.atleta[i].pontos > 0):
                    atletaPontos = '<span class="data-score up">' + data.atleta[i].pontos.toFixed(2) + '</span>';
                    playerboxUp++;
                    break;

                  case (data.atleta[i].pontos < 0):
                    atletaPontos = '<span class="data-score down">' + data.atleta[i].pontos.toFixed(2).toString().replace('-', '') + '</span>';
                    playerboxDown++;
                    break;

                  case (data.atleta[i].pontos === 0):
                    atletaPontos = '<span class="data-score zero">' + data.atleta[i].pontos.toFixed(2) + '</span>';
                    playerboxZero++;
                    break;

                  default:
                    atletaPontos = '<span class="data-score null"></span>';
                    playerboxNull++;
                    break;
                }

                // Playerbox
                $('[data-playerbox-up]').text(playerboxUp);
                $('[data-playerbox-down]').text(playerboxDown);
                $('[data-playerbox-zero]').text(playerboxZero);
                $('[data-playerbox-null]').text(playerboxNull);

                // Table rows
                tableBody += '<tr><td class="align-center"><span class="tag tag-position tag-position-' + data.atleta[i].posicao.abreviacao.toLowerCase() + '">' + data.atleta[i].posicao.abreviacao + '</span></td><td><strong>' + data.atleta[i].apelido + '</strong><br><span class="text-xsmall text-uppercase">' + data.atleta[i].clube.nome + '</span></td><td class="align-center">' + atletaPontos + '</td></tr>';
              }

              $table_body.append(tableBody);
            }
          },

          complete: function () {
            main.base.teamSelect();
            main.base.viewerShow(true);
            main.base.loaderShow(false);
          }
        });
      });
    },

    partial: function(offline) {
      var that = this;

      main.base.viewer('views/partial.html', function () {
        var $action_team_select = $(main.base.action_team_select);

        if (offline) {
          $(main.base.global_viewer).prepend('<div class="alert" data-alert-close="true"><p><strong> Ops! O Cartola FC parece estar em manutenção...</strong></p></div>');
          main.alert.build();
        }

        $('.league-team-total').text(main.setup.getLeagueTeamList().length);

        main.base.teamSelect();
        main.base.viewerShow(true);
      });
    },

    filterPlayerPosition: function($this) {
      var $tableBodyRows = $('.main-content tbody tr'),
          position = +$this.attr('href').replace('#', ''),
          onClass = 'on';

      if ($tableBodyRows.length) {
        if (position in main.base.playerPosition) {
          $tableBodyRows.hide().filter(':has(.tag-position-' + main.base.playerPosition[position].short + ')').show();
        } else {
          $tableBodyRows.show();
        }

        $this.parent().addClass(onClass).siblings().removeClass(onClass);
      }
    },

    bindings: function () {
      var that = this,
        $action_ranking = $(main.base.action_ranking);

      // Action reload
      $(document).on({
        click: function () {
          main.base.viewerReload();
        }
      }, main.base.action_reload);

      // Action previous
      $(document).on({
        click: function () {
          main.base.viewerPrevious();
        }
      }, main.base.actionPrevious);

      // Action ranking
      $action_ranking.on({
        change: function () {
          that.build();
        }
      });

      // Action partial players
      $(document).on({
        click: function () {
          that.playersPartial();
        }
      }, main.base.action_partial_players);

      // Action partial round
      $(document).on({
        click: function () {
          that.build(true);
        }
      }, main.base.action_partial_round);

      // Action partial ranking
      $(document).on({
        click: function () {
          that.leaguePartialRanking();
        }
      }, main.base.action_partial_ranking);

      // Action team select
      $(document).on({
        change: function () {
          that.team($(this).val());
        }
      }, main.base.action_team_select);

      // Action team table row
      $(document).on({
        click: function () {
          that.team($(this).data('viewTeam'));
        }
      }, '[data-view-team]');

      // Filter player position
      $(document).on({
        click: function (e) {
          that.filterPlayerPosition($(this));
          e.preventDefault();
        }
      }, main.base.filterPlayerPosition + ' a');
    },

    init: function () {
      var that = this;

      that.build();
      that.bindings();
    }
  };
}(jQuery, this, this.document));
