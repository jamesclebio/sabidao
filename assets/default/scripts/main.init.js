;(function($, window, document, undefined) {
  'use strict';

  main.init = function() {
    var plugins = [
      'goTop',
      'alert',
      'setup'
    ];

    if (arguments.length) {
      plugins = arguments;
    }

    for (var i in plugins) {
      this[plugins[i]].init();
    }
  };

  main.init();
}(jQuery, this, this.document));
