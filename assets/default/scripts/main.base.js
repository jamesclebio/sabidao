;(function($, window, document, undefined) {
  'use strict';

  main.base = {
    xhr: null,
    history: {
      previous: null,
      current: null
    },
    league_team_list: [],
    league_team_list_temp: [],
    league_team_list_partial: [],
    league_team_pass: true,
    team_formation: {
      1: '3-4-3',
      2: '3-5-2',
      3: '4-3-3',
      4: '4-4-2',
      5: '4-5-1',
      6: '5-3-2',
      7: '5-4-1'
    },
    playerPosition: {
      1: {
        short: 'gol',
        full: 'Goleiro',
      },
      2: {
        short: 'lat',
        full: 'Lateral',
      },
      3: {
        short: 'zag',
        full: 'Zagueiro',
      },
      4: {
        short: 'mei',
        full: 'Meia',
      },
      5: {
        short: 'ata',
        full: 'Atacante',
      },
      6: {
        short: 'tec',
        full: 'Técnico'
      }
    },
    action_reload: '.action-reload',
    actionPrevious: '.action-previous',
    action_ranking: '.action-ranking',
    action_partial_players: '.action-partial-players',
    action_partial_round: '.action-partial-round',
    action_partial_ranking: '.action-partial-ranking',
    action_team_select: '.action-team-select',
    filterPlayerPosition: '.filter-player-position',
    global_viewer: '.global-viewer',
    global_loader: '.global-loader',
    global_error: '.global-error',

    xhrClear: function() {
      if (this.xhr && this.xhr.readystate != 4) {
        this.xhr.abort();
      }
    },

    viewer: function(view, callback, reload) {
      var that = this,
        $global_viewer = $(that.global_viewer);

      that.xhrClear();
      that.viewerShow(false);
      that.loaderShow(false);
      that.errorShow(false);

      // Clear league team list temp
      that.league_team_list_temp = [];

      if (!reload) {
        that.history.previous = that.history.current;
        that.history.current = {
          view: view,
          callback: callback
        };
      }

      $.get(view, function(data) {
        $global_viewer.empty().append(data);

        that.xhr = callback();
      });
    },

    viewerReload: function() {
      this.viewer(this.history.current.view, this.history.current.callback, true);
    },

    viewerPrevious: function() {
      this.viewer(this.history.previous.view, this.history.previous.callback);
    },

    viewerShow: function(on) {
      var that = this,
        $global_viewer = $(that.global_viewer);

      if (on) {
        $global_viewer.show();
      } else {
        $global_viewer.hide();
      }
    },

    navEnable: function(on) {
      var that = this,
        $action_reload = $(that.action_reload),
        $action_ranking = $(that.action_ranking);

      if (on) {
        $action_reload.removeAttr('disabled');
        $action_ranking.removeAttr('disabled');
      } else {
        $action_reload.attr('disabled', 'disabled');
        $action_ranking.attr('disabled', 'disabled');
      }
    },

    loaderShow: function(on, text, button) {
      var that = this,
        $global_loader = $(that.global_loader),
        $text = $global_loader.find('.text'),
        $action = $global_loader.find('.action');

      if (text) {
        $text.html(text);
      } else {
        $text.empty();
      }

      if (button) {
        $action.html(button);
      } else {
        $action.empty();
      }

      if (on) {
        $global_loader.show();
        that.navEnable(false);
      } else {
        $text.empty();
        $global_loader.hide();
        that.navEnable(true);
      }
    },

    errorShow: function(on, text) {
      var that = this,
        $global_viewer = $(that.global_viewer),
        $global_error = $(that.global_error),
        $text = $global_error.find('.text');

      if (text) {
        $text.html(text);
      }

      if (on) {
        $global_viewer.empty();
        $global_error.show();
      } else {
        $text.empty();
        $global_error.hide();
      }
    },

    leagueTeamList: function(data) {
      var that = this,
          position_previous = 0;

      for (var i in data) {
        if (data[i].posicao > position_previous) {
          that.league_team_list.push({
            posicao: data[i].posicao,
            variacao: '-',
            time: {
              nome: data[i].time.nome,
              slug: data[i].time.slug,
              nome_cartola: data[i].time.nome_cartola
            }
          });
        }

        position_previous = data[i].posicao;
      }
    },

    teamSelect: function() {
      var that = this,
        $action_team_select = $(that.action_team_select),
        league_team_list = main.setup.getLeagueTeamList();

      for (var i in league_team_list) {
        $action_team_select.append('<option value="' + league_team_list[i].time.slug + '|' + league_team_list[i].time.nome + '|' + league_team_list[i].time.nome_cartola + '">' + league_team_list[i].time.nome + '</option>');
      }
    },
  };
}(jQuery, this, this.document));
